import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class TestChrome {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("This is a test with Chrome Browser");

        System.setProperty("webdriver.chrome.driver","C:\\dev\\tools\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://orangehrm-demo-6x.orangehrmlive.com/auth/login");

        //Login Page
        driver.findElement(By.id("txtUsername")).clear();
        driver.findElement(By.id("txtUsername")).click();  //right button --> Inspect element
        driver.findElement(By.id("txtUsername")).sendKeys("Admin");
        driver.findElement(By.id("txtPassword")).clear();
        driver.findElement(By.id("txtPassword")).click();
        driver.findElement(By.id("txtPassword")).sendKeys("admin123");
        driver.findElement(By.name("Submit")).click();  //right button --> Inspect element


        //Dashboard
        Thread.sleep(5000);
        driver.manage().window().maximize();
        driver.findElement(By.linkText("PIM")).click();

       /* Thread.sleep(5000);
        driver.findElement(By.linkText("Employee List")).click();
        Thread.sleep(5000);
        List<WebElement> employees = driver.findElements(By.xpath("//td[@ng-click='vm.viewProfileIfHasAccessablePimTabs(employee)' and @class='nowrap cursor-pointer']"));

        if (employees.size()>6){
            employees.get(7).click();
        }*/


        //PIM options -- Add employee
        Thread.sleep(5000);
        driver.findElement(By.linkText("Add Employee")).click();
        Thread.sleep(16000);

        driver.findElement(By.id("firstName")).click();
        driver.findElement(By.id("firstName")).sendKeys("Juan");
        driver.findElement(By.id("lastName")).click();
        driver.findElement(By.id("lastName")).sendKeys("Perez");
       /* driver.findElement(By.xpath("//input[@class='select-dropdown' and @value='-- Select --']")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//ul[@class='dropdown-content select-dropdown']")).click();*/
        Select drpCountry = new Select (driver.findElement(By.xpath("//input[@class='select-dropdown' and @value='-- Select --']")).click());
        drpCountry.selectByVisibleText("     Australian Regional HQ");


        Thread.sleep(15000);
        driver.quit();

    }
}
