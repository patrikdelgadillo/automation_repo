import org.openqa.selenium.WebDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.WebDriverException;

public class TestOpera {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("This is a Test with Opera Browser");

        OperaOptions options = new OperaOptions();
        options.setBinary("C:\\\\Users\\Gerson\\AppData\\Local\\Programs\\Opera\\56.0.3051.104\\opera.exe");

        System.setProperty("webdriver.opera.driver","C:\\\\dev\\tools\\operadriver.exe");
        WebDriver driver = new OperaDriver(options);
        driver.get("https://google.com");
       // driver.get("http://www.google.com");

        Thread.sleep(15000);
        driver.quit();
    }
}
