import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

public class TaskOne {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Task number One - Search the word Robot");

        System.setProperty("webdriver.gecko.driver","C:\\dev\\tools\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.google.com");

        driver.findElement(By.id("lst-ib")).click();  //right button --> Inspect element
        driver.findElement(By.id("lst-ib")).sendKeys("Robot");
        driver.findElement(By.name("btnK")).click();  //right button --> Inspect element

        //driver.findElement(By.className("LC20lb")).click();
        List<WebElement> links = driver.findElements(By.className("LC20lb"));

        links.get(2).click();
        Thread.sleep(15000);

        List<WebElement> menu = driver.findElements(By.className("masthead-nav-topics-anchor"));
        menu.get(2).click();

        Thread.sleep(15000);
        driver.quit();
    }
}
