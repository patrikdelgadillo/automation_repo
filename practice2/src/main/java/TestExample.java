import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

public class TestExample {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("This is an Example of Automation");

        System.setProperty("webdriver.gecko.driver","C:\\\\dev\\tools\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.google.com");

        driver.findElement(By.xpath("//div/input[@class='gLFyf gsfi']")).click();  //right button --> Inspect element
        driver.findElement(By.xpath("//div/input[@class='gLFyf gsfi']")).sendKeys("Naruto");

        List<WebElement> searchButton = driver.findElements(By.xpath("//*[@name='btnK']"));
        //searchButton.get(1).click();
        if (searchButton.size() > 0){
            if (searchButton.get(1) != null){
                searchButton.get(1).click();
            }
        }

        //driver.findElement(By.xpath("//*[@name='btnK']")).click();  //right button --> Inspect element

        driver.findElement(By.linkText("Imágenes")).click();
        List<WebElement> imagenes = driver.findElements(By.tagName("img"));

       // if (imagenes.size()>2){
            imagenes.get(3).click();
       // }

        Thread.sleep(15000);
        driver.quit();
    }
}
