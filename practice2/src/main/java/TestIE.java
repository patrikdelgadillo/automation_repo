import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class TestIE {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("This is a test with IE browser");

        System.setProperty("webdriver.ie.driver","C:\\\\dev\\tools\\IEDriverServer.exe");
        WebDriver driver = new InternetExplorerDriver();
        driver.get("http://www.google.com");

        driver.findElement(By.name("q")).click();
        driver.findElement(By.name("q")).sendKeys("One Piece");

        driver.findElement(By.name("btnG")).click();  //right button --> Inspect element

        driver.findElement(By.linkText("One Piece - Wikipedia, la enciclopedia libre")).click();

        //driver.findElement(By.linkText("Escenario principal")).click();


        Thread.sleep(15000);
        driver.close();
        driver.quit();
    }
}
