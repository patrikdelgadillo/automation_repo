package test;

import framework.manager.BrowserManager;
import framework.pages.OrangeHRM.EmployeeListPage;
import framework.pages.OrangeHRM.LoginPage;
import org.testng.annotations.Test;

public class EmployeeListTest extends TestBase{

    @Test
    public void testLoginOrangeHRM() throws InterruptedException {
        LoginPage loginPage = new LoginPage();

        loginPage
                .goTo()
                .clickOnLoginButton().menuPim.clickOnPimMainMenu().clickOnEmployeeListSubOption();

        EmployeeListPage employeeListPage = new EmployeeListPage();
        employeeListPage.selectEmployeeFromTheList();


    }
}
