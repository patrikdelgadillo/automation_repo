package framework.pages.OrangeHRM;

import framework.pages.BasePage;
import framework.reports.ReportManager;
import framework.utils.CommonActions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MenuPim extends BasePage {
    @FindBy (linkText = "PIM")
    private WebElement pimMainMenu;

    @FindBy(linkText = "Employee List")
    private WebElement employeeListSubOption;

    public MenuPim clickOnPimMainMenu() {
        try {
            Thread.sleep(10000);
            CommonActions.click(pimMainMenu);
            ReportManager.getInstance().logTestPass("Click on PIM option on Main Menu");
        } catch (Exception e) {
            ReportManager.getInstance().logTestFailed("Step Failed");
        }
        return this;
    }

    public MenuPim clickOnEmployeeListSubOption() {
        try {
            CommonActions.click(employeeListSubOption);
            ReportManager.getInstance().logTestPass("Click on Employee List sub-Option");
        } catch (Exception e) {
            ReportManager.getInstance().logTestFailed("Step Failed");
        }
        return this;
    }
}
