package framework.pages.OrangeHRM;

import framework.pages.BasePage;
import framework.reports.ReportManager;
import framework.utils.CommonActions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class EmployeeListPage extends BasePage {
    @FindBy(xpath = "//td[@ng-click='vm.viewProfileIfHasAccessablePimTabs(employee)' and @class='nowrap cursor-pointer']")
    private List<WebElement> employeeList;

    public EmployeeListPage selectEmployeeFromTheList() throws InterruptedException {
        try{
            Thread.sleep(5000);
            CommonActions.click(employeeList.get(7));
            ReportManager.getInstance().logTestPass("Click on one Employee from the List");
        }catch (Exception e){
            ReportManager.getInstance().logTestFailed(e.getMessage());
        }
        return this;
    }
}
